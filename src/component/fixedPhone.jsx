import { Component, UISmartPanelGrid, UIText } from "rainbowui-desktop-core";
import PropTypes from 'prop-types';
import '../css/component.css';
import { StringUtil, Util, r18n } from "rainbow-desktop-tools";
import { ValidatorContext } from "rainbow-desktop-cache";

export default class TwFixedPhone extends Component {
    constructor(props) {
        super(props)
        this.state = {
            phoneLength: "5",
            oldValue: '',
            FixedPhone: '',
            dateModel: { AreaCode: '', Phone: '', Extension: '' },
            required: true
        }
        this.areaId = "fp-area-" + this.generateId();
        this.phoneId = "fp-phone-" + this.generateId();
        this.fixedValidationGroup = this.props.validationGroup ? this.props.validationGroup : "fixedValidation"
    }

    generateId() {
        let index = new Date().getTime();
        return "rainbow-ui-" + (index++);
    }

    async componentDidMount() {
        let modelObj = this.props.model[this.props.property];
        if (modelObj) {
            let arr = modelObj.split(this.props.split);
            let AreaCode = arr[0];
            let phoneLength = 5;
            if (AreaCode && AreaCode.length == 2) {
                phoneLength = 8;
            } else if (AreaCode && AreaCode.length == 3) {
                phoneLength = 7;
            } else if (AreaCode && AreaCode.length == 4) {
                phoneLength = 8;
            }
            this.setState({ dateModel: { AreaCode: arr[0], Phone: arr[1], Extension: arr[2] }, phoneLength })
        } else {
            if (!Util.parseBool(this.props.required)) {
                this.setState({ required: false });
            }
        }
        let spanRequired = $('#' + this.areaId + '_required');
        if (spanRequired && spanRequired.length > 0) {
            if (!Util.parseBool(this.props.required)) {
                spanRequired.css("display", 'none');
                await ValidatorContext.removeValidator(this.props.validationGroup, this.areaId);
                await ValidatorContext.removeValidator(this.props.validationGroup, this.phoneId);
                this.clearChangeValidationInfo(this.props);
            } else {
                spanRequired.css("display", '')
            }
        }
    }

    clearChangeValidationInfo(nextProps) {
        const areaObject = $("#" + this.areaId);
        const phoneObject = $("#" + this.phoneId);

        if (nextProps && nextProps.required != undefined) {
            //area
            areaObject.parent().parent().next().remove();
            areaObject.parent().parent().next().remove(); // small
            areaObject.parent().parent().find(".glyphicon-remove").remove();
            if (areaObject.parents('.has-feedback').length > 0 || areaObject.parents('.has-error').length > 0) {
                $(areaObject.parents('.has-feedback')[0]).removeClass('has-feedback');
                $(areaObject.parents('.has-error')[0]).removeClass('has-error')
            }
            const errorAreaObject = areaObject.closest(".form-group");
            if (errorAreaObject.hasClass("has-error")) {
                areaObject.closest(".input-group").css("border", "2px solid #E1E8EE");
            }
            //phone
            phoneObject.parent().next().remove();
            phoneObject.parent().next().remove(); // small
            areaObject.parent().find(".glyphicon-remove").remove();
            if (phoneObject.parents('.has-feedback').length > 0 || phoneObject.parents('.has-error').length > 0) {
                $(phoneObject.parents('.has-feedback')[0]).removeClass('has-feedback');
                $(phoneObject.parents('.has-error')[0]).removeClass('has-error')
            }
            const errorPhoneObject = phoneObject.closest(".form-group");
            if (errorPhoneObject.hasClass("has-error")) {
                phoneObject.closest(".input-group").css("border", "2px solid #E1E8EE");
            }
        }
    }

    async componentDidUpdate(prevProps) {
        let spanRequired = $('#' + this.areaId + '_required');
        if (spanRequired && spanRequired.length > 0) {
            if (!Util.parseBool(this.props.required)) {
                spanRequired.css("display", 'none')
            } else {
                spanRequired.css("display", '')
            }
        }
        if (prevProps.required != this.props.required) {
            if(!Util.parseBool(this.props.required)) {
                let validationGroupList = prevProps.validationGroup ? prevProps.validationGroup.split(',') : [];
                if (validationGroupList.length > 0) {
                    _.each(validationGroupList, async (validationId) => {
                        await ValidatorContext.removeValidator(validationId, this.areaId);
                        await ValidatorContext.removeValidator(validationId, this.phoneId);
                    })
                }
            }
            this.clearChangeValidationInfo(prevProps);
        }
    }

    async componentWillReceiveProps(nextProps) {
        this.state.required = nextProps.required;
        this.fixedValidationGroup = nextProps.validationGroup ? nextProps.validationGroup : "fixedValidation"
        this.calcModel(nextProps);
    }

    calcModel(nextProps) {
        let modelObj = nextProps.model[nextProps.property];
        if (modelObj) {
            let arr = modelObj.split('-');
            this.state.dateModel.AreaCode = arr[0];
            this.state.dateModel.Phone = arr[1];
            this.state.dateModel.Extension = arr[2];
        }
        if (modelObj == null || modelObj == '') {
            this.state.dateModel.AreaCode = '';
            this.state.dateModel.Phone = '';
            this.state.dateModel.Extension = '';
        }
    }

    render() {
        let labelNoI18n = this.props.label ? false : true;
        // const _phone = this.state.FixedPhone.split(this.props.split);

        // this.AreaCode = _phone[0];
        // this.Phone = _phone[1];
        // this.Extension = _phone[2];


        // if (this.props.layout && this.props.layout == "horizontal") {
        //     return (
        //         <div id="desktop-fixed-phone" class="fixed-phone-horizontal">
        //             <UISmartPanelGrid column="12">
        //                 <UIText id={this.areaId} model={this.state.dateModel} enabled={this.props.enabled} label={this.props.label ? this.props.label : r18n.fixedPhone} colspan="3" widthAllocation="5,7" noI18n={labelNoI18n} allowChars="0123456789" layout='horizontal' property="AreaCode" maxLength="4" onChange={this.changeFixedPhone.bind(this)} required={Util.parseBool(this.props.required)} validationGroup={this.fixedValidationGroup}/>
        //                 <UIText id={this.phoneId} model={this.state.dateModel} enabled={this.props.enabled} label={r18n.and} colspan="6" noI18n="true" layout='horizontal' widthAllocation="1,11" allowChars="0123456789" property="Phone" maxLength={this.state.phoneLength} onChange={this.changeFixedPhone.bind(this)} required="true" validationGroup={this.fixedValidationGroup}/>
        //                 <UIText model={this.state.dateModel} enabled={this.props.enabled} label={r18n.and} colspan="3" noI18n="true" layout='horizontal' widthAllocation="1,11" allowChars="0123456789" property="Extension" maxLength="6" onChange={this.changeFixedPhone.bind(this)}/>
        //             </UISmartPanelGrid>
        //         </div>
        //     )
        // } else {
        return (
            <div id="desktop-fixed-phone" class="fixed-phone-vertical">
                <UISmartPanelGrid column="12">
                    <UIText id={this.areaId} model={this.state.dateModel} enabled={this.props.enabled} label={this.props.label ? this.props.label : r18n.fixedPhone} colspan="3" widthAllocation="1,11" noI18n={labelNoI18n} allowChars="0123456789" layout={this.props.layout} widthAllocation="0,12" property="AreaCode" maxLength="4" onChange={this.changeFixedPhone.bind(this)} required={this.state.required} validationGroup={this.fixedValidationGroup} />
                    <UIText id={this.phoneId} model={this.state.dateModel} enabled={this.props.enabled} label={r18n.and} colspan="5" noI18n="true" layout='horizontal' lableShowDiv={true} labelDivStyle={{ width: '100%', height: '1px', background: '#000', marginTop: '17px' }} widthAllocation="1,11" allowChars="0123456789" property="Phone" maxLength={this.state.phoneLength} onChange={this.changeFixedPhone.bind(this)} required={this.state.required} validationGroup={this.fixedValidationGroup} />
                    <UIText model={this.state.dateModel} enabled={this.props.enabled} label={r18n.and} colspan="4" noI18n="true" layout='horizontal' lableShowDiv={true} labelDivStyle={{ width: '100%', height: '1px', background: '#000', marginTop: '17px' }} widthAllocation="1,11" allowChars="0123456789" property="Extension" maxLength="6" onChange={this.changeFixedPhone.bind(this)} />
                </UISmartPanelGrid>
            </div>
        )
        // }
    }

    async changeFixedPhone() {
        let AreaCode = this.state.dateModel.AreaCode;
        let oldPhoneLength = this.state.phoneLength;
        let phoneLength = 5;
        if (AreaCode && AreaCode.length == 2) {
            phoneLength = 8;
        } else if (AreaCode && AreaCode.length == 3) {
            phoneLength = 7;
        } else if (AreaCode && AreaCode.length == 4) {
            phoneLength = 8;
        }

        let Phone = this.state.dateModel.Phone ? this.state.dateModel.Phone.toString() : '';
        let Extension = this.state.dateModel.Extension ? this.state.dateModel.Extension.toString() : '';
        let FixedPhone = null;
        if (StringUtil.isEmpty(AreaCode) || oldPhoneLength > phoneLength) {
            Phone = '';
            if (!Util.parseBool(this.props.required)) {
                this.setState({ required: false });
            }
        } else {
            FixedPhone = AreaCode + this.props.split + Phone;
            if (!StringUtil.isEmpty(Extension)) {
                FixedPhone = FixedPhone + this.props.split + Extension;
            }
            this.setState({ required: true })
        }
        this.props.model[this.props.property] = FixedPhone;
        this.setState({ FixedPhone: FixedPhone, phoneLength: phoneLength, dateModel: { AreaCode, Phone, Extension } })
        if (this.props.onChange) {
            this.props.onChange();
        }
        let validatorList = ValidatorContext.getValidatorList();
        let validatorObj = null;
        let fixedValidationGroupList = this.fixedValidationGroup ? this.fixedValidationGroup.replace(/\s*/g, '').split(',') : [];
        if (validatorList && validatorList.length > 0 && fixedValidationGroupList && fixedValidationGroupList.length > 0) {
            _.each(validatorList, async (validator) => {
                _.each(fixedValidationGroupList, async (groupItem) => {
                    if (groupItem == validator.validatorId) {
                        validatorObj = Object.assign({}, validatorObj, validator.validator);

                    }
                })
            })
        }
        if (validatorObj && (StringUtil.isNotEmpty(AreaCode) || StringUtil.isNotEmpty(Phone))) {
            let validate = {};
            for (let key in validatorObj) {
                if (key.indexOf("fp-phone") > -1) {
                    validate[key] = validatorObj[key];
                }
                if (key.indexOf("fp-area") > -1) {
                    validate[key] = validatorObj[key];
                }
            }
            await ValidatorContext.validate("true", this.fixedValidationGroup, undefined, validate, undefined);
        } else {
            await ValidatorContext.validate("true", this.fixedValidationGroup, undefined, {}, undefined);
        }
    }

};

TwFixedPhone.propTypes = $.extend({}, Component.propTypes, {
    label: PropTypes.string,
    split: PropTypes.string,
    required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    layout: PropTypes.oneOf(["horizontal", "vertical"])
});


TwFixedPhone.defaultProps = $.extend({}, Component.defaultProps, {
    required: false,
    split: '-',
    layout: 'vertical'
});
